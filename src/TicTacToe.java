import java.util.Scanner;

public class TicTacToe {
    static int turnCount = 9;
    static int row, col;
    static char player = 'X';
    static char[][] tables = {
            { '-', '-', '-' },
            { '-', '-', '-' },
            { '-', '-', '-' }
    };

    public static void main(String[] args) throws Exception {
        homeText();
        playGame();
    }

    private static void homeText() {
        System.out.println("Welcome to Tic-Tac-Toe Game!");
    }

    private static void checkContinue() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Do you want to continue? (y/n)");
        if (sc.nextLine().equalsIgnoreCase("y")) {
            restartGame();
        } else {
            sc.close();
        }
    }

    private static void restartGame() {
        turnCount = 9;
        player = 'X';
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                tables[i][j] = '-';
            }
        }
        playGame();
    }

    private static void playGame() {
        while (turnCount > 0) {
            showTables();
            inputProcess();
            turnCount--;
            if (isVictory(tables)) {
                showTables();
                System.out.println("Player " + player + " WIN!");
                break;
            }
            switchPlayer();
        }
        if (!isVictory(tables)) {
            System.out.println("Draw!");
        }
        checkContinue();
    }

    private static char switchPlayer() {
        return player = (player == 'X') ? 'O' : 'X';
    }

    public static boolean isVictory(char[][] tables) {
        for (int i = 0; i < 3; i++) {
            if (tables[i][0] == player && tables[i][1] == player && tables[i][2] == player) {
                return true;
            }
        }
        for (int i = 0; i < 3; i++) {
            if (tables[0][i] == player && tables[1][i] == player && tables[2][i] == player) {
                return true;
            }
        }
        if (tables[0][0] == player && tables[1][1] == player && tables[2][2] == player) {
            return true;
        }
        if (tables[0][2] == player && tables[1][1] == player && tables[2][0] == player) {
            return true;
        }
        return false;
    }

    private static void inputProcess() {
        System.out.print("Please input Row and Column : ");
        Scanner sc = new Scanner(System.in);
        try {
            row = sc.nextInt() - 1;
            col = sc.nextInt() - 1;
            if (tables[row][col] != '-') {
                System.err.println("This spot has been taken, Please try again");
                inputProcess();
            } else {
                tables[row][col] = player;
            }
        } catch (Exception e) {
            System.out.println("Your input is not correct, Please try again");
            showTables();
            inputProcess();
        }
    }

    private static void showTables() {
        for (char[] row : tables) {
            for (char column : row) {
                System.out.printf("%3c", column);
            }
            System.out.println();
        }
        System.out.println("Player " + player + " turn");
        System.out.println("Turn left : " + turnCount);
    }
}
