
import org.junit.Assert;
import org.junit.Test;

public class testTicTacToe {
    @Test
    public void testVictoryRow1(){
        char[][] tables = {{'X','X','X'},{'-','-','-'},{'-','-','-'},};
        Assert.assertEquals(true, TicTacToe.isVictory(tables));
    }
    @Test
    public void testVictoryRow2(){
        char[][] tables = {{'-','-','-'},{'X','X','X'},{'-','-','-'},};
        Assert.assertEquals(true, TicTacToe.isVictory(tables));
    }
    @Test
    public void testVictoryRow3(){
        char[][] tables = {{'-','-','-'},{'-','-','-'},{'X','X','X'},};
        Assert.assertEquals(true, TicTacToe.isVictory(tables));
    }
    @Test
    public void testVictoryCol1(){
        char[][] tables = {{'X','-','-'},{'X','-','-'},{'X','-','-'},};
        Assert.assertEquals(true, TicTacToe.isVictory(tables));
    }
    @Test
    public void testVictoryCol2(){
        char[][] tables = {{'-','X','-'},{'-','X','-'},{'-','X','-'},};
        Assert.assertEquals(true, TicTacToe.isVictory(tables));
    }
    @Test
    public void testVictoryCol3(){
        char[][] tables = {{'-','-','X'},{'-','-','X'},{'-','-','X'},};
        Assert.assertEquals(true, TicTacToe.isVictory(tables));
    }
    @Test
    public void testVictoryDiagonal1(){
        char[][] tables = {{'X','-','-'},{'-','X','-'},{'-','-','X'},};
        Assert.assertEquals(true, TicTacToe.isVictory(tables));
    }
    @Test
    public void testVictoryDiagonal2(){
        char[][] tables = {{'-','-','X'},{'-','X','-'},{'X','-','-'},};
        Assert.assertEquals(true, TicTacToe.isVictory(tables));
    }
    @Test
    public void testDraw1(){
        char[][] tables = {{'-','-','O'},{'-','X','-'},{'X','-','-'},};
        Assert.assertEquals(false, TicTacToe.isVictory(tables));
    }
    @Test
    public void testDraw2(){
        char[][] tables = {{'O','-','X'},{'-','O','-'},{'X','-','O'},};
        Assert.assertEquals(false, TicTacToe.isVictory(tables));
    }
    @Test
    public void testDraw3(){
        char[][] tables = {{'O','O','O'},{'-','X','X'},{'X','-','-'},};
        Assert.assertEquals(false, TicTacToe.isVictory(tables));
    }
} 
